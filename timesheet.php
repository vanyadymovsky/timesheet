<?php


namespace Clerk;

/**
 * Carefully modify
 * USER_ID,
 * OAUTH_TOKEN,
 * CLIENT_LOGIN,
 * CLIENT_PASSWORD,
 * IS_IBM
 */
const USER_ID = '8ca28ebc-ec7d-28e1-ed94-4e2452ea92e2';

const OAUTH_TOKEN = 'e866b1c2-ef99-0c6f-c270-540454a04fe0';

const BASE_URL = 'https://sugarinternal.sugarondemand.com/';

const CLIENT_URL = 'https://sugarinternal.sugarondemand.com/';

const CLIENT_LOGIN = 'IDymovsky';

const CLIENT_PASSWORD = 'Sugardymovsky7';

const IS_IBM = true;



class ParserException extends \Exception{}

class Parser
{
    public function parse($stream)
    {
        $date = null;
        $timesheets = array();
        while ($line = fgets($stream)) {
            $line = trim($line);
            if ($line === '') {
                $date = null;
            } else if ($date === null) {
                $date = $this->parseDate($line);
            } else {
                if (!$date) {
                    throw new ParserException('Date is undefined');
                }

                list($subject, $spent) = $this->parseTimesheet($line);
                $timesheets[] = $this->createTimesheet($subject, $date, $spent);
            }
        }
        return $timesheets;
    }

    private function parseDate($line)
    {
        try {
            return new \DateTime($line);
        } catch (Exception $e) {
            throw new ParserException(
                sprintf('Unable to parse date: %s', $line),
                null,
                $e
            );
        }
    }

    private function parseTimesheet($line)
    {
        $parts = explode(' - ', $line, 2);
        if (count($parts) < 2) {
            throw new ParserException(sprintf('Timesheet line has wrong format: %s', $line));
        }

        return $parts;
    }

    private function createTimesheet($subject, $date, $spent)
    {
        return new Timesheet($subject, $date, $spent);
    }
}

class View
{
    public function __construct($dateFormat)
    {
        $this->dateFormat = $dateFormat;
    }

    function display(Timesheet $timesheet, $i)
    {
        printf('Timesheet #%02d' . PHP_EOL, $i);
        printf('Date: %s' . PHP_EOL, $timesheet->getDate()->format($this->dateFormat));
        printf('Subject: %s' . PHP_EOL, $timesheet->getSubject());
        printf('Spent: %d' . PHP_EOL, $timesheet->getSpent());
        printf('Activity: %s' . PHP_EOL, $timesheet->getActivity()->getName());
        printf('Task: %s' . PHP_EOL, $timesheet->getActivity()->getTask()->getName());
        printf(PHP_EOL);
    }
}

class Client
{
    private $baseUrl = BASE_URL;
    private $userId = USER_ID;
    private $oAuthToken = OAUTH_TOKEN;

    public function __construct($url, $username, $password)
    {
    }

    public function send(Timesheet $timesheet)
    {
        $message = $this->format($timesheet);
        $message = json_encode($message);
        $taskId = $timesheet->getActivity()->getTask()->getID();
        // POST /rest/v10/Tasks/:task_id/link/tasks_ps_timesheets_1
        // OAuth-Token: $oAuthToken
        $cmd = <<<CMD
curl {$this->baseUrl}rest/v10/Tasks/{$taskId}/link/tasks_ps_timesheets_1 \
  -D - \
  -H "Content-Type: application/json" \
  -H "OAuth-Token: {$this->oAuthToken}" \
  -d @- << 'EOF'
$message
EOF
CMD;
        echo $cmd . PHP_EOL;
    }

    protected function format(Timesheet $timesheet)
    {
        $activity = $timesheet->getActivity();
        $subject = $timesheet->getSubject();
        $date = $timesheet->getDate();
        $name = sprintf('%s (%s)', $subject, $date->format('m/d/Y'));

        return array(
            'activity_type' => $activity->getName(),
            'tasks_ps_timesheets_1tasks_ida' => $activity->getTask()->getID(),
            'assigned_user_id' => $this->userId,
            'name' => $name,
            'description' => $subject,
            'activity_date' => $date->format('Y-m-d'),
            'time_spent' => $timesheet->getSpent(),
        );
    }
}

class Timesheet
{
    private $subject;
    private $date;
    private $spent;
    private $activity;

    public function __construct($subject, \DateTime $date, $spent)
    {
        $this->subject = $subject;
        $this->date = $date;
        $this->spent = $spent;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getSpent()
    {
        return $this->spent;
    }

    public function getActivity()
    {
        return $this->activity;
    }

    public function setActivity(Activity $activity)
    {
        $this->activity = $activity;
    }
}

abstract class Category
{
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }
}

class Task extends Category
{
    private $id;

    public function __construct($name, $id)
    {
        parent::__construct($name);
        $this->id = $id;
    }

    public function getID()
    {
        return $this->id;
    }
}

class Activity extends Category
{
    private $name;

    private $task;

    private $keywords = array();

    public function __construct($name, Task $task, array $keywords = array())
    {
        parent::__construct($name);
        $this->task = $task;
        $this->keywords = $keywords;
    }

    public function getTask()
    {
        return $this->task;
    }

    public function test(Timesheet $timesheet)
    {
        $subject = $timesheet->getSubject();
        foreach ($this->keywords as $keyword) {
            if (stripos($subject, $keyword) !== false) {
                return true;
            }
        }

        return false;
    }

    public function update(Timesheet $timesheet)
    {
        $timesheet->setActivity($this);
    }
}

class Updater
{
    public function __construct(array $activities, Activity $defaultActivity)
    {
        $this->activities = $activities;
        $this->defaultActivity = $defaultActivity;
    }

    public function update(Timesheet $timesheet)
    {
        foreach ($this->activities as $activity) {
            if ($activity->test($timesheet)) {
                $activity->update($timesheet);
                return;
            }
        }

        $this->defaultActivity->update($timesheet);
    }
}

if (IS_IBM) {
    $ibmPoc = new Task('IBM POC', '643e34da-c349-4599-f2fc-4d27acfccf89');
    $projectSupport = new Activity('Billable', $ibmPoc);

    $updater = new Updater(array(), $projectSupport);
} else {
    $bugFixing = new Task('Bugfixing', 'b86344ae-3602-7465-fd50-4ec4dd28a3ef');
    $generalActivities = new Task('General Activities', '94bf8529-9ea3-ac44-57d2-4ec4dda60ec9');
    $projectSupport = new Activity('Project Support', $bugFixing);
    $interview = new Activity('Interview', $generalActivities, array('interview'));
    $internalMeetings = new Activity('Internal Meetings', $generalActivities, array('meeting', 'discussion'));

    $updater = new Updater(array($interview, $internalMeetings), $projectSupport);
}

// same dir
$stream = fopen('./TimeSheet.txt', 'r');
$parser = new Parser();
$view = new View('d M, l');
$client = new Client(CLIENT_URL, CLIENT_LOGIN, CLIENT_PASSWORD);

foreach ($parser->parse($stream) as $timesheet) {
    $updater->update($timesheet);
    $client->send($timesheet);
    //$view->display($timesheet, $i);
}
